		<footer id="main-footer">
			<div class="container">
				<div class="row">
					<div class="col-xs-6 col-sm-3 footer-menu">
						<h3>Otros servicios para tus viajes</h3>
            <?php nw_footer_first_col_nav(); ?>
					</div>
					<div class="col-sm-3 hidden-xs footer-menu">
						<h3>Más Destinos Obligados</h3>
            <?php 
            $obligados_args = array(
              'posts_per_page' => 5,
              'offset' => 0,
              'tag' => 'destino-obligado',
              'orderby' => 'date',
              'order' => 'DESC'
              );

            $obligados_query = new WP_Query($obligados_args);
            ?>
            <?php if($obligados_query->have_posts()) : ?>
              <ul>
              <?php while ($obligados_query->have_posts()) : $obligados_query->the_post(); ?>
                <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
              <?php endwhile; ?>
              </ul>
            <?php endif; ?>
          </div>
          <div class="col-sm-3 hidden-xs footer-menu">
            <h3>Más Shopping</h3>
            <?php 
            $shopping_args = array(
              'posts_per_page' => 5,
              'offset' => 0,
              'tag' => 'shopping',
              'orderby' => 'date',
              'order' => 'DESC'
              );

            $shopping_query = new WP_Query($shopping_args);
            ?>
            <?php if($shopping_query->have_posts()) : ?>
              <ul>
              <?php while ($shopping_query->have_posts()) : $shopping_query->the_post(); ?>
                <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
              <?php endwhile; ?>
              </ul>
            <?php endif; ?>
					</div>
					<div class="col-xs-6 col-sm-3 footer-menu">
						<h3>Aún no has visto todo</h3>
						<?php  nw_footer_main_nav(); ?>
            <img src="<?php bloginfo('template_url'); ?>/img/assets/logo.png">
					</div>
				</div>
			</div>
      <div class="legales">
        <div class="container">
          <div class="row">
            <div class="col-sm-6 copyright">
              <p>TRAVESÍAS.COM © 2015 | DERECHOS RESERVADOS</p>
            </div>
            <div class="col-sm-6 nuevaweb">
              <p>SITIO WEB DISEÑADO Y DESARROLLADO POR <a href="http://www.nuevaweb.com.mx" target="_blank">NUEVAWEB</a></p>
            </div>
          </div><!--.row-->
        </div><!--.container-->
      </div><!--.legales-->
		</footer>
		
		<?php // Load styles and scripts from functions.php nw_enqueue_scripts() function ?>
		
		<?php wp_footer(); // wordpress admin-bar functions ?>
</body>
</html>