<?php // DOCS: http://justintadlock.com/archives/2009/05/26/the-complete-guide-to-creating-widgets-in-wordpress-28
/**
 * Plugin name: Example Widget
 * Plugin URI: http://example.com/widget
 * Description: A widget that serves as an example for developing more advanced widgets.
 * Version: 0.1
 * Author: Justin Tadlock
 * Author URI: http://justintadlock.com
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/**
 * Add function to widgets_init that'll load our widget.
 * @since 0.1
 */
add_action( 'widgets_init', 'region_order_widget' );

/**
 * Register our widget.
 * 'Region_order_widget' is the widget class used below.
 *
 * @since 0.1
 */
function region_order_widget() {
	register_widget( 'Region_order_widget' );
}

/**
 * Example Widget class.
 * This class handles everything that needs to be handled with the widget:
 * the settings, form, display, and update.  Nice!
 *
 * @since 0.1
 */
class Region_order_widget extends WP_Widget {

	/**
	 * Widget setup.
	 */
	function Region_order_widget() {
		/* Widget settings. */
		$widget_ops = array( 
			'classname' => 'RegionOrder', 
      'description' => __('Widget para organizar las distintas regiones de viajes', 'Region_order_widget') 
      );

		/* Widget control settings. */
		$control_ops = array( 
      'width' => 300, 
      'height' => 350, 
      'id_base' => 'region-order' 
      );

		/* Create the widget. */
		$this->WP_Widget( 'region-order', __('Region Order', 'RegionOrder'), $widget_ops, $control_ops );
	}

	/**
	 * How to display the widget on the screen.
	 */
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$region = apply_filters('widget_region', $instance['region'] );
		$columns = $instance['columns'];
    ?>
    
    <?php $region_category = get_category_by_slug($region); 
      $region_ID = $region_category->term_id;
      $region_name = $region_category->name;
    ?>
    <?php
    $regiones_query_args = array(
      'posts_per_page' => $columns,
      'orderby' => 'date',
      'order' => "DESC",
      'category_name' => $region_name
    );
    $regiones_query = new WP_Query($regiones_query_args) ?>
    <?php if ($regiones_query->have_posts()) :?>
      <div class="<?php echo $columns > 1 ? 'col-xs-12' : 'col-xs-6'; ?> ">
        <section id="<?php echo $region_category->slug; ?>">
          <div class="row">
            <div class="mapa col-xs-12<?php echo $columns > 1 ? ' col-sm-2' : ' col-sm-4'; ?>">
              <h2><?php echo $region_name; ?></h2>
              <i class="fa fa-angle-right"></i>
            </div>
            <div class="col-xs-12 <?php echo $columns > 1 ? 'col-sm-10':'col-sm-8'; ?>">
              <div class="row">
                <?php while($regiones_query->have_posts()) : $regiones_query->the_post(); 
                  $post_id = get_the_ID();
                  if(has_post_thumbnail()) {
                    $post_thumbnail = wp_get_attachment_thumb_url( get_post_thumbnail_id($post_id) );                    
                  }
                  $tags = get_the_tags();
                  $tag_slug = $tags[0]->slug;
                  $tag_name = $tags[0]->name;
                  ?>
                  <article class="col-xs-12<?php echo $columns > 1 ? ' col-sm-6'.($columns == 4 ? ' col-md-3':' col-md-4') : ''; ?> <?php echo $tag_slug ? ($tag_slug == 'shopping' ? 'shopping':'obligado'):''; ?>">
                    <a href="<?php the_permalink(); ?>">
                      <div class="post-meta">
                        <?php if($tag_slug) { ?>
                            <p class="post-tag"><?php echo $tag_name ?></p>
                        <?php } ?>
                        <h1 class="post-title"><?php the_title(); ?></h1>
                      </div><!--.post-meta-->
                    <div class="thumb-img" style="background-image:url('<?php echo has_post_thumbnail() ? ''.$post_thumbnail.'':''. bloginfo('template_url') .'/img/background/no-thumb.jpg'; ?>'); width:100%; height:160px;"> 
                      <img src="<?php bloginfo('template_url'); ?>/img/background/pixel.png" style="width:100%; height:160px;">
                    </div>
                    </a>
                    <div class="content">
                      <?php the_excerpt();?>
                    </div>
                    <div class="cta">
                      <a href="<?php the_permalink(); ?>" class="itinerario">VER ITINERARIO</a>
                      <a href="#" class="reservaciones">RESERVACIONES</a>
                    </div>
                  </article>
                <?php endwhile; ?>
              </div><!--.row-->
            </div><!--.col-xs-12 col-sm-10 col-md-9-->
          </div><!--.row-->
        </section>
      </div>
      <?php else: ?>
    <?php endif; ?>

  <?php
	}

	/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for region and columns to remove HTML (important for text inputs). */
		$instance['region'] = strip_tags( $new_instance['region'] );
		$instance['columns'] = strip_tags( $new_instance['columns'] );

		return $instance;
	}

	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_columns() function
	 * when creating your form elements. This handles the confusing stuff.
	 */
	function form( $instance ) {

		/* Set up some default widget settings. */
		$defaults = array( 
			'region' => 'america', 
			'columns' => '4',
			);
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Region: Select Box -->
    <p>Selecciona la región a mostrar</p>
    <p>
      <label for="<?php echo $this->get_field_id( 'region' ); ?>"><?php _e('Category Name:', 'PostTypeFeed'); ?></label> 
      <select id="<?php echo $this->get_field_id( 'region' ); ?>" name="<?php echo $this->get_field_name( 'region' ); ?>" class="widefat" style="width:100%;">
        <option>All Categories</option>
        <?php
        $args = array(
          'orderby' => 'name',
          'order' => 'ASC'
          );
        $categories = get_categories($args);
          foreach($categories as $category) { 
            if ($instance['region'] == $category->name) {
              echo '<option selected="selected">'. $category->name .'</option>';
            }else{
              echo '<option>'. $category->name . '</option>';
            }
          }// end foreach 
        ?>
      </select>
    </p>
		<!-- Your columns: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'columns' ); ?>"><?php _e('Your columns:', 'example'); ?></label>
			<input id="<?php echo $this->get_field_id( 'columns' ); ?>" name="<?php echo $this->get_field_name( 'columns' ); ?>" value="<?php echo $instance['columns']; ?>" style="width:100%;" />
		</p>

	<?php
	}
}

?>