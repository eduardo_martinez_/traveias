<?php
/*
Template Name: [Destinos]
*/
?>

<?php get_header(); ?>
<div class="destinos">
  <div class="mapamundi">
    <div id="overlay">
      <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d49001783.65474466!2d-30.779758600933864!3d41.44287721125048!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2smx!4v1447100783625" id="map" width="100%" height="400px" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
  </div><!--.mapa-->

  <div class="regiones">
    <div class="container">
      <div class="row">
        <?php include_once('world-feed.php'); ?>
      </div><!--.row-->
    </div><!--.container-->
  </div><!--.regiones-->
</div><!--.destinos-->
<?php get_footer(); ?>

