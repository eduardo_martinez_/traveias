(function($) {
	
	function mainNavContainer() {
		var windowWidth = $(window).width();
		if(windowWidth > 992) {
			$('.main-header-container').addClass('container');
			$('.main-header-container').removeClass('container-fluid');
		}else {
			$('.main-header-container').removeClass('container');
			$('.main-header-container').addClass('container-fluid');
		}
	}
	function conversorTitleHeight() {
		var conversorHeight = $('.conversor-index #wp-currency-converter-2').height();
		$('.conversor-index #wp-currency-converter-2 .title').addClass('col-sm-2 col-xs-12');
		
		if ($(window).width() >= 768) {
			$('.conversor-index #wp-currency-converter-2 .title').css('height', conversorHeight);
		} else {
			$('.conversor-index #wp-currency-converter-2 .title').css('height', 50);
		}
	}

	function footerHeight() {
		var maxHeight = 0;
		var windowWidth = $(window).width();
		if(windowWidth>767) {
			$('.footer-menu').css({height:'auto'});
			$('.footer-menu').each(function(){
				var heightFooter = $(this).height();
				if(heightFooter>maxHeight){
					maxHeight=heightFooter;
				}
			});
			$('.footer-menu').css('height', maxHeight);
		} else {
			$('.footer-menu').css({height:'auto'});
		}
	}

	function mapInteraction() {
    $('#map').addClass('scrolloff');                // set the mouse events to none when doc is ready    
    $('#overlay').on("mouseup",function(){          // lock it when mouse up
        $('#map').addClass('scrolloff'); 
        //somehow the mouseup event doesn't get call...
    });
    $('#overlay').on("mousedown",function(){        // when mouse down, set the mouse events free
        $('#map').removeClass('scrolloff');
    });
    $("#map").mouseleave(function () {              // becuase the mouse up doesn't work... 
        $('#map').addClass('scrolloff');            // set the pointer events to none when mouse leaves the map area                                                    // or you can do it on some other event
    });
	}

	$(document).ready(function(){
		mainNavContainer();
		footerHeight();
		conversorTitleHeight();
		mapInteraction();
		$('[data-toogle="tooltip"]').tooltip({container:'body'});
	});

	$(window).resize(function(){
		mainNavContainer();
		footerHeight();
		conversorTitleHeight();
	});

	$(window).load(function() {
		conversorTitleHeight();
	});


})( jQuery );