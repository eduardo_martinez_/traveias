<?php get_header(); ?>

<div class="single-destinos">
	<?php if (have_posts()) : ?> 
		<?php while (have_posts()) : the_post(); ?>
			<?php 
      $current_post_id = get_the_ID();

      if(has_post_thumbnail()) {
        $post_thumbnail = wp_get_attachment_thumb_url( get_post_thumbnail_id($post_id) );                    
      } ?>
			<div class="container">
				<div class="row">
          <div class="col-xs-12">
            <header>
    					<div class="col-xs-12 col-sm-4 title">
    						<h1><?php the_title(); ?></h1>
    					</div>
    					<div class="col-xs-12 col-sm-8 feature-img" style="background-image:url('<?php echo has_post_thumbnail() ? ''.$post_thumbnail.'':''. bloginfo('template_url') .'/img/background/no-thumb.jpg'; ?>')">
    					</div>
            </header>
          </div>
				  <div class="clearfix"></div>
					<div class="col-xs-12 col-sm-8">
            <div class="content">
							<?php the_content(); ?>
            </div><!--.content-->
					</div><!--.col-xs-12.col-sm-8-->

					<div class="col-xs-12 col-sm-4">
            <?php $post_meta_pais = get_post_custom_values('pais'); ?>
            <?php $post_meta_moneda = get_post_custom_values('moneda'); ?>
            <?php $post_meta_clima = get_post_custom_values('clima'); ?>
            <div class="sidebar1">
              <?php if ($post_meta_pais || $post_meta_id || $post_meta_id) { ?>
                <?php if($post_meta_pais) { ?>
                  <p>País:</p>
                  <h3><?php echo $post_meta_pais[0]; ?></h3>
                <?php } ?>

                <?php if($post_meta_moneda) { ?>
                  <p>Moneda:</p>
                  <h3><?php echo $post_meta_moneda[0]; ?></h3>
                <?php } ?>

                <?php if($post_meta_clima) { ?>
                  <p>Clima:</p>
                  <?php 
                  $title = get_the_title();
                  echo do_shortcode(
                    '[wunderground location="'. $post_meta_clima[0] .'" numdays="3" layout="simple" showdata="search,icon,highlow,daynames" location_title="'. $title. '" layout="table-vertical" numdays="5" measurement="c" language="SP"]'
                    );
                } ?>
              <?php } ?>

              <?php if(is_active_sidebar ( 'conversor')) :?>
                <?php dynamic_sidebar( 'conversor' ); ?>
              <?php endif; ?>
              <div class="mapamundi" style="margin-top:30px;">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d60028037.70552596!2d-83.86569610093386!3d23.322264785050585!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2smx!4v1447101704421" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
              </div><!--.mapa-->


            </div><!--.sidebar-->
            <div clas="clearfix"></div>
            <div class="sidebar2">
              <?php 
              $current_category = get_the_category();
              $current_category_slug = $current_category[0]->slug;
              $post_excluido = array($current_post_id);
              $sugeridos_args = array(
                'posts_per_page' => 4,
                'category_name' => $current_category_slug,
                'post__not_in' => $post_excluido,
                'orderby' => 'rand',
                'order' => 'DESC'
                );
              
              $sugeridos_query = new WP_Query($sugeridos_args);
              ?>
              <h3>Otros destinos recomendados para ti:</h3>
              <?php if ($sugeridos_query->have_posts()) :?>
                <?php while($sugeridos_query->have_posts()) : $sugeridos_query->the_post(); 
                    if(has_post_thumbnail()) {
                          $post_thumbnail = wp_get_attachment_thumb_url( get_post_thumbnail_id($post_id) );                    
                    } ?>
                    <article>
                      <a href="<?php the_permalink(); ?>">
                        <div class="post-meta">
                          <h1 class="post-title"><?php the_title(); ?></h1>
                        </div><!--.post-meta-->
                        <div class="thumb-img" style="background-image:url('<?php echo has_post_thumbnail() ? ''.$post_thumbnail.'':''. bloginfo('template_url') .'/img/background/no-thumb.jpg'; ?>'); width:100%; height:160px;"> 
                          <img src="<?php bloginfo('template_url'); ?>/img/background/pixel.png" style="width:100%; height:160px;">
                        </div>
                      </a>
                    </article>
                <?php endwhile; ?>
              <?php else:
                $sugeridos2_args = array(
                  'posts_per_page' => 4,
                  'post__not_in' => $post_excluido,
                  'orderby' => 'rand',
                  'order' => 'DESC'
                  );    

                $sugeridos2_query = new WP_Query($sugeridos2_args); ?>

                <?php if ($sugeridos2_query->have_posts()) :?>
                <?php while($sugeridos2_query->have_posts()) : $sugeridos2_query->the_post(); 
                    if(has_post_thumbnail()) {
                          $post_thumbnail = wp_get_attachment_thumb_url( get_post_thumbnail_id($post_id) );                    
                    } ?>
                    <article>
                      <a href="<?php the_permalink(); ?>">
                        <div class="post-meta">
                          <h1 class="post-title"><?php the_title(); ?></h1>
                        </div><!--.post-meta-->
                        <div class="thumb-img" style="background-image:url('<?php echo has_post_thumbnail() ? ''.$post_thumbnail.'':''. bloginfo('template_url') .'/img/background/no-thumb.jpg'; ?>'); width:100%; height:160px;"> 
                          <img src="<?php bloginfo('template_url'); ?>/img/background/pixel.png" style="width:100%; height:160px;">
                        </div>
                      </a>
                    </article>
                <?php endwhile;
                endif;?>
            <?php endif; ?>
            </div>
					</div><!--.col-xs-12.xol-sm-4-->
				</div><!--.row-->
			</div><!--.container-->

		<?php endwhile; ?>
		 
	<?php else : ?>
	<?php endif; ?>
</div>

<?php get_footer(); ?>