<?php get_header(); ?>
<?php $category_name = get_the_category();
$current_category = $category_name[0]->slug; ?>

<div class="category">
  <div class="mapamundi">
    <div id="overlay">
      <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d49001783.65474466!2d-30.779758600933864!3d41.44287721125048!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2smx!4v1447100783625" id="map" width="100%" height="520px" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
  </div><!--.mapa-->

    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-4 <?php echo $current_category ?>">
          <div class="row description">
                <h1><?php single_cat_title(); ?></h1>
          </div><!--.row-->
        </div><!--.col-xs-4-->
        <div class="col-xs-12 col-sm-8 feature-post">
          <div class="row">
            <?php
              $feature_post_args = array (
                'posts_per_page' => 1,
                'category_name' => $current_category,
                'meta_key' => 'feature',
                'orderby' => 'date',
                'order' => 'DESC'
                );

              $feature_post_query = new WP_Query($feature_post_args); ?>

              <?php 
              if($feature_post_query->have_posts()) : ?>
                <?php while($feature_post_query->have_posts()) : $feature_post_query->the_post() ?>
                  <?php $feature_ID = get_the_ID(); ?>
                  <?php if(has_post_thumbnail()) {
                        $post_thumbnail = wp_get_attachment_thumb_url( get_post_thumbnail_id($post_id) );                    
                  } ?>
                  <a href="<?php the_permalink(); ?>" class="col-xs-12 col-sm-6 feature_post_thumb" 
                    style="background-image:url('<?php echo has_post_thumbnail() ? ''.$post_thumbnail.'':''. bloginfo('template_url') .'/img/background/no-thumb.jpg'; ?>')">
                  </a>
                  <div class="col-xs-12 col-sm-6">
                    <a href="<?php the_permalink(); ?>" class="post-title">
                    <div class="row">
                      <div class="feature_post_tag">Destino Principal</div>
                    </div>
                      <h1 class="post-title"><?php the_title(); ?></h1>
                    </a>
                    <div class="content"><?php the_excerpt();?></div>
                    <div class="cta">
                      <a href="<?php the_permalink(); ?>" class="itinerario">VER ITINERARIO</a>
                      <a href="#" class="reservaciones">RESERVACIONES</a>
                    </div><!--.cta-->
                  </div><!--.col-sm-4-->
                <?php endwhile; ?>
              <?php else: 
                $no_feature_post_args = array (
                'posts_per_page' => 1,
                'category_name' => $current_category
                );
                ?>
                <?php $no_feature_post_query = new WP_Query($no_feature_post_args);
                  if($no_feature_post_query->have_posts()) : ?>
                    <?php while($no_feature_post_query->have_posts()) : $no_feature_post_query->the_post() ?>
                      <?php $feature_ID = get_the_ID(); ?>
                      <?php if(has_post_thumbnail()) {
                            $post_thumbnail = wp_get_attachment_thumb_url( get_post_thumbnail_id($post_id) );                    
                      } ?>
                      <a href="<?php the_permalink(); ?>" class="col-xs-12 col-sm-6 feature_post_thumb" 
                        style="background-image:url('<?php echo has_post_thumbnail() ? ''.$post_thumbnail.'':''. bloginfo('template_url') .'/img/background/no-thumb.jpg'; ?>')">
                      </a>
                      <div class="col-xs-12 col-sm-6">
                        <a href="<?php the_permalink(); ?>" class="post-title">
                          <div class="row">
                            <div class="feature_post_tag">Destino Principal</div>
                          </div>
                          <h1><?php the_title(); ?></h1>
                        </a>
                        <div class="content"><?php the_excerpt();?></div>
                        <div class="cta">
                          <a href="<?php the_permalink(); ?>" class="itinerario">VER ITINERARIO</a>
                          <a href="#" class="reservaciones">RESERVACIONES</a>
                        </div><!--.cta-->
                      </div><!--.col-sm-4-->
                    <?php endwhile; ?>
                  <?php else:
                  endif;
                endif;
              wp_reset_postdata();
            ?>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
      <?php 
      $post_excluido = array($feature_ID);

      $regiones_query_args = array(
            'posts_per_page' => -1,
            'post__not_in' => $post_excluido,
            'orderby' => 'date',
            'order' => "ASC",
            'category_name' => $current_category
          );
      $regiones_query = new WP_Query($regiones_query_args) ?>
      <?php if ($regiones_query->have_posts()) :?>
        <div class="row">
          <div class="col-xs-12">
            <div class="row">
              <?php while($regiones_query->have_posts()) : $regiones_query->the_post(); 
                $post_id = get_the_ID();
                $tags = get_the_tags();
                $tag_slug = $tags[0]->slug;
                $tag_name = $tags[0]->name;
                if(has_post_thumbnail()) {
                      $post_thumbnail = wp_get_attachment_thumb_url( get_post_thumbnail_id($post_id) );                    
                } ?>
                <article class="col-xs-12 col-sm-6 col-md-4<?php echo $tag_slug ? ($tag_slug == 'shopping' ? ' shopping':' obligado'):''; ?>">
                  <a href="<?php the_permalink(); ?>">
                    <div class="post-meta">
                      <?php if($tag_slug) { ?>
                        <p class="post-tag"><?php echo $tag_name=='feature' ? '':''.$tag_name.''; ?></p>
                      <?php } ?>
                      <h1 class="post-title"><?php the_title(); ?></h1>
                    </div><!--.post-meta-->
                    <div class="thumb-img" style="background-image:url('<?php echo has_post_thumbnail() ? ''.$post_thumbnail.'':''. bloginfo('template_url') .'/img/background/no-thumb.jpg'; ?>'); width:100%; height:160px;"> 
                      <img src="<?php bloginfo('template_url'); ?>/img/background/pixel.png" style="width:100%; height:160px;">
                    </div>
                  </a>
                  <div class="content">
                    <?php the_excerpt();?>
                  </div>
                  <div class="cta">
                    <a href="<?php the_permalink(); ?>" class="itinerario">VER ITINERARIO</a>
                    <a href="#" class="reservaciones">RESERVACIONES</a>
                  </div>
                </article>
              <?php endwhile; ?>
            </div><!--.row-->
          </div><!--.col-sm-12-->
        </div><!--.row-->
      <?php else: ?>
    <?php endif; ?>
  </div><!--.container-->
</div><!--.category-->
<?php get_footer(); ?>
