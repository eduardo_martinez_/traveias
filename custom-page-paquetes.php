<?php
/*
Template Name: [Paquetes]
*/
?>

<?php get_header(); ?>

<div class="page_paquetes">
	<?php if (have_posts()) : ?> 
		<?php while (have_posts()) : the_post(); 
      $current_post_id = get_the_ID(); 
			if(has_post_thumbnail()) {
	        $post_thumbnail = wp_get_attachment_thumb_url( get_post_thumbnail_id($post_id) );                    
      } ?>
			<div class="container">
          <header>
            <div class="col-xs-12 col-sm-6 title">
              <h1><?php the_title(); ?></h1>
            </div>
            <div class="col-xs-12 col-sm-6 feature-img" style="background-image:url('<?php echo has_post_thumbnail() ? ''.$post_thumbnail.'':''. bloginfo('template_url') .'/img/background/no-thumb.jpg'; ?>')">
            </div>
          </header>
          <div class="clearfix"></div>
          <div class="col-xs-12 col-sm-8">
				    <div class="row">
              <div class="content">
  							<?php the_content(); ?>
              </div><!--.content-->
            </div>
					</div><!--.col-xs-12-->

          <div class="row">
  					<div class="col-xs-12 col-sm-4">
              <div class="sidebar2">
                <h3>Conoce los paquetes que tenemos para ti:</h3>
    						<?php 
                  $paquetes_array = array(
                    'posts_per_page' => 4,
                    'category_name' => 'paquetes',
                    'orderby' => 'rand',
                    'order' => 'DESC'
                    );

                  $paquetes_query = new WP_Query($paquetes_array);
                ?>
                <?php if ($paquetes_query->have_posts()) :?>
                    <?php while($paquetes_query->have_posts()) : $paquetes_query->the_post(); 
                        if(has_post_thumbnail()) {
                              $post_thumbnail = wp_get_attachment_thumb_url( get_post_thumbnail_id($post_id) );                    
                        } ?>
                        <article>
                          <a href="<?php the_permalink(); ?>">
                            <div class="post-meta">
                              <h1 class="post-title"><?php the_title(); ?></h1>
                            </div><!--.post-meta-->
                            <div class="thumb-img" style="background-image:url('<?php echo has_post_thumbnail() ? ''.$post_thumbnail.'':''. bloginfo('template_url') .'/img/background/no-thumb.jpg'; ?>'); width:100%; height:160px;"> 
                              <img src="<?php bloginfo('template_url'); ?>/img/background/pixel.png" style="width:100%; height:160px;">
                            </div>
                          </a>
                        </article>
                    <?php endwhile; ?>
                  <?php else:
                    $sugeridos2_args = array(
                      'posts_per_page' => 4,
                      'post__not_in' => $post_excluido,
                      'orderby' => 'rand',
                      'order' => 'DESC'
                      );    

                    $sugeridos2_query = new WP_Query($sugeridos2_args); ?>

                    <?php if ($sugeridos2_query->have_posts()) :?>
                    <?php while($sugeridos2_query->have_posts()) : $sugeridos2_query->the_post(); 
                        if(has_post_thumbnail()) {
                              $post_thumbnail = wp_get_attachment_thumb_url( get_post_thumbnail_id($post_id) );                    
                        } ?>
                        <article>
                          <a href="<?php the_permalink(); ?>">
                            <div class="post-meta">
                              <h1 class="post-title"><?php the_title(); ?></h1>
                            </div><!--.post-meta-->
                            <div class="thumb-img" style="background-image:url('<?php echo has_post_thumbnail() ? ''.$post_thumbnail.'':''. bloginfo('template_url') .'/img/background/no-thumb.jpg'; ?>'); width:100%; height:160px;"> 
                              <img src="<?php bloginfo('template_url'); ?>/img/background/pixel.png" style="width:100%; height:160px;">
                            </div>
                          </a>
                        </article>
                    <?php endwhile;
                    endif;?>
                <?php endif; ?>
              </div><!--sidebar-->
  					</div><!--.col-xs-12.col-sm-8-->
          </div><!--.row-->
			</div><!--.container-->

		<?php endwhile; ?>
		 
	<?php else : ?>
	<?php endif; ?>
</div>

<?php get_footer(); ?>