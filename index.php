<?php get_header(); ?>
<?php if(function_exists('putRevSlider')){
        putRevSlider("index","homepage");
}?>
<?php if(is_active_sidebar( 'slogan' )) :?>
	<div class="slogan">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
				<?php dynamic_sidebar( 'slogan' ); ?>
				</div><!--.col-sm-12-->
			</div><!--.row-->
		</div><!--.container-->
	</div><!--.slogan-->
<?php endif; ?>

<div class="regiones">
  <div class="container">
    <div class="row">
      <?php include_once('world-feed.php'); ?>

      <?php if(is_active_sidebar ( 'clima')) : ?>
        <div class="col-xs-12 weather">
          <div class="visible-xs col-xs-12 titulo">
            <h2>Conoce el pronóstico del clima de tu próxima aventura.</h2>
          </div>
          <div class="col-xs-12 col-sm-6 pronostico">
            <?php dynamic_sidebar( 'clima' ); ?>
          </div><!--.col-xs-12.col-sm-6-->
          <div class="hidden-xs col-sm-6 titulo">
            <h2>Conoce el pronóstico del clima de tu próxima aventura.</h2>
          </div>
        </div><!--.col-xs-12.col-sm-6-->
      <?php endif; ?>

      <?php if(is_active_sidebar ( 'conversor')) :?>
        <div class="col-xs-12">
          <div class="row conversor-index ">
            <?php dynamic_sidebar( 'conversor' ); ?>
          </div>
        </div><!--.conversor-index-->
      <?php endif; ?>
    </div><!--.row-->
  </div><!--.container-->
</div><!--.regiones-->




<?php get_footer(); ?>