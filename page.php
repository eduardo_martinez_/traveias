<?php get_header(); ?>
<div class="page">
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-10">
          <article>

          <header>

          <h1><?php the_title(); ?></h1>

          </header>

          <?php the_content(); ?>

          </article>
        </div><!--.col-xs-12.col-sm-10-->
      </div><!--.row-->
    </div><!--.container-->

  <?php endwhile; else : ?>
  <?php endif; ?>

</div>
<?php get_footer(); ?>

